# Archives 2019

## 2019-08

* 2019-08-27 +28+29 [Entrée libre](https://www.infothema.fr/forum/index.php/topic,1969.0.html), Quimper 

## 2019-09

* 2019-09-26 [Siivim](https://siivim.fr/), Nevers

## 2019-10

* 2019-10-02 [We Love Green IT](https://evenium.net/ng/person/event/website.jsf?eventId=1l74b033&page=tickets&loc=fr&justSubmit=false&cid=32741), Paris
* 2019-10-09 [Gitlab Commit](https://gitlabcommit2019london.sched.com/), London
* 2019-10-10 [OVHCloud Summit](https://summit.ovhcloud.com/fr/), Paris
* 2019-10-24 +25; [ForumPHP 2019](https://event.afup.org/), Paris, 275€ 
* 2019-10-25 +26+27, [MozFest](https://www.mozillafestival.org/fr/tickets/), London, 45£ 