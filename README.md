# Planning conférences

Planning des conférences & événements

# 2019

## 2019-11

* 2019-11-5 + 6 [Decode](https://decodeproject.eu/events/our-data-our-future-radical-tech-democratic-digital-society), Turin IT 
* 2019-11-06 [CRIP Opensource](https://www.crip-asso.fr/crip/event/detail.html/idConf/755), Paris Ministère des Finances (DGFIP)
* 2019-11-07 +08 [Rencontres Geotrek 2019 (PDF)](https://geotrek.ecrins-parcnational.fr/rencontres/2019/Rencontres-Geotrek-2019-Programme.pdf), Nimes, [inscriptions avant le 22/10/19](https://framaforms.org/inscription-rencontres-geotrek-2019-1569231468)
* 2019-11-13 +14+15 [Siivim Quebec](http://siivim.shawinigan.ca/), Shawinigan
* 2019-11-13 +14, [Devops D-Day](http://2019.devops-dday.com/), Marseille http://2019.devops-dday.com/ 100€
* 2019-11-14 [LibDay](https://2019.libday.fr/programme/) (couplé à Devops D-Day), Marseille, 
* 2019-11-16 +17, [Capitole du Libre](https://capitoledulibre.org/), Toulouse, 
* **2019-11-19 +20+21, [Salon des maires](http://www.salondesmaires.com/)**, Paris, 

## 2019-12

* 2019-12-03 +04 +05 +06 [JRes 2019](https://www.jres.org/), Dijon
* 2019-12-05 /!\ grève possible ([source](https://www.alternatives-economiques.fr/regimes-speciaux-de-resistance/00090413))
* **2019-12-10 +11 [POSS](https://www.opensourcesummit.paris/)**, Paris
* 2019-12-10 +11 [API days](https://www.apidays.co/paris2019), Paris 
* 2019-12-12 [NetSecureDay](https://www.netsecure-day.fr/), Rouen

# 2020

## 2010-01

* 2020-01-30 [SustainOSS](https://sustainoss.org/) Bruxelles

## 2020-02

* 2020-02-01 +02 [Fosdem](https://fosdem.org/2020/), Bruxelles 

## 2020-08

* 2020-08-21 +22 [Euruko 2020](https://euruko2020.org/), Helsinki, FINLAND


